using RSMInventory.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RSMInventory.Repos
{

    //Usage example :
        // public interface IModelRepository : IRepository<Model>
        //{


        //}

        //public class ModelRepository : Repository<Model>, IModelRepository
        //{
            //public IList<Model> GetAllRedModels() 
            //{
            //     return GetList(model => model.Color.Equals("Red").ToList();   
            //}

        //}

    public class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        public Repository()
        {
        }

        private DataContext dc;

        public Repository(DataContext context)
        {
            dc = context;

        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        public virtual void Add(params T[] items)
        {
            Update(items);
        }


        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
           
                IQueryable<T> dbQuery = dc.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList<T>();
            
            return list;
        }

        

        public virtual IList<T> GetList(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
          
                IQueryable<T> dbQuery = dc.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();
            
            return list;
        }


        public virtual void ExecStoredProcCommand(string query, params object[] parameters)
        {
           dc.ExecuteCommand(query, parameters);
        }

        //Queryable Implementation
        //public virtual IQueryable<T> GetList(Func<T, bool> where,
        //     params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    IQueryable<T> list;
        //    using (var context = new M2M7DataClassesDataContext())
        //    {
        //        //IQueryable<T> dbQuery = context.Set<T>();
        //        IQueryable<T> dbQuery = context.GetTable<T>();

        //        //Apply eager loading
        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        list = dbQuery
        //            .AsNoTracking()
        //            .Where(where).AsQueryable();
        //            //.ToList<T>();
        //    }
        //    return list;
        //}



        public virtual T GetSingle(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;
            
                IQueryable<T> dbQuery = dc.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
            
            return item;
        }



       //Probably not working right
        public virtual void Update(params T[] items)
        {
            
            using (var context = dc)
            {

                dc.SubmitChanges();
                //IQueryable<T> dbQuery = context.Set<T>()
                //var r = context.GetTable<T>();
                //foreach (T item in items)
                //{
                //    //dbSet.Add(item);
                //    foreach (var entry in context.GetChangeSet().Updates)
                //    {
                        
                //        var entity = entry;
                //       // entry.State = GetEntityState(entity.EntityState);
                //    }
                //}
                //dc.SubmitChanges();
            }
        }


        /// <summary>
        /// Create a new instance of type T.
        /// </summary>
        /// <returns></returns>
        public virtual T CreateInstance()
        {
            T entity = Activator.CreateInstance<T>();
            dc.GetTable<T>().InsertOnSubmit(entity);
            
            return entity;
        }


        public virtual void Remove(params T[] items)
        {
            Update(items);
        }

        public void Dispose()
        {
            dc.Dispose();
        }



        #region Properties

        private string PrimaryKeyName
        {
            get { return TableMetadata.RowType.IdentityMembers[0].Name; }
        }

        private System.Data.Linq.Table<T> GetTable
        {
            get { return dc.GetTable<T>(); }
        }

        private System.Data.Linq.Mapping.MetaTable TableMetadata
        {
            get { return dc.Mapping.GetTable(typeof(T)); }
        }

        private System.Data.Linq.Mapping.MetaType ClassMetadata
        {
            get { return dc.Mapping.GetMetaType(typeof(T)); }
        }

        #endregion

        //protected static System.Data.Entity.EntityState GetEntityState(Data.EntityState entityState)
        //{
        //    switch (entityState)
        //    {
        //        case Data.EntityState.Unchanged:
        //            return System.Data.Entity.EntityState.Unchanged;
        //        case Data.EntityState.Added:
        //            return System.Data.Entity.EntityState.Added;
        //        case Data.EntityState.Modified:
        //            return System.Data.Entity.EntityState.Modified;
        //        case Data.EntityState.Deleted:
        //            return System.Data.Entity.EntityState.Deleted;
        //        default:
        //            return System.Data.Entity.EntityState.Detached;
        //    }
        //}



    }
}
